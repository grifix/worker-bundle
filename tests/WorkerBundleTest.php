<?php

declare(strict_types=1);

namespace Grifix\WorkerBundle\Tests;

use Grifix\Worker\WorkerFactoryInterface;
use Grifix\WorkerBundle\Tests\Dummies\RunWorkerCommand;
use Symfony\Bundle\FrameworkBundle\Console\Application;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use Symfony\Component\HttpKernel\KernelInterface;

final class WorkerBundleTest extends KernelTestCase
{
    protected static function getKernelClass(): string
    {
        return Kernel::class;
    }

    protected static function createKernel(array $options = []): KernelInterface
    {
        /**
         * @var Kernel $kernel
         */
        $kernel = parent::createKernel($options);
        TestHelper::setUpKernel($kernel);
        $kernel->handleOptions($options);

        return $kernel;
    }

    public function testItWorks(): void
    {

        $kernel = self::bootKernel();
        $application = new Application($kernel);
        /** @var WorkerFactoryInterface $workerFactory */
        $workerFactory = self::getContainer()->get(WorkerFactoryInterface::class);
        $application->addCommands([
            new RunWorkerCommand($workerFactory)
        ]);
        $application->setAutoExit(false);
        TestHelper::setData(0);
        TestHelper::executeCommand(
            $application,
            [
                'command' => 'test:run-worker',
                '--run-limit' => 5
            ],
        );
        self::assertEquals(5, TestHelper::getData());
    }
}

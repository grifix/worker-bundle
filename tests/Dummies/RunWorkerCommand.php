<?php

declare(strict_types=1);

namespace Grifix\WorkerBundle\Tests\Dummies;

use Grifix\Worker\AbstractWorkerCommand;
use Grifix\Worker\WorkerFactoryInterface;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

final class RunWorkerCommand extends AbstractWorkerCommand
{
    public const NAME = 'test:run-worker';
    protected static $defaultName = self::NAME;
    protected static $defaultDescription = 'test command';

    private readonly Worker $worker;

    public function __construct(WorkerFactoryInterface $workerFactory)
    {
        parent::__construct($workerFactory);
        $this->worker = new Worker();
    }

    protected function getCallback(InputInterface $input, OutputInterface $output): callable
    {
        return function () {
            $this->worker->__invoke();
        };
    }
}

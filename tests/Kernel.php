<?php

declare(strict_types=1);

namespace Grifix\WorkerBundle\Tests;

use Nyholm\BundleTest\TestKernel;

final class Kernel extends TestKernel
{
    public function getCacheDir(): string
    {
       return __DIR__.'/var/cache';
    }

    public function getLogDir(): string
    {
        return __DIR__.'/var/log';
    }
}

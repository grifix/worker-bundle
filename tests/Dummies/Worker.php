<?php

declare(strict_types=1);

namespace Grifix\WorkerBundle\Tests\Dummies;


use Grifix\WorkerBundle\Tests\TestHelper;

final class Worker
{

    public function __invoke(): void
    {
        /** @var int $data */
        $data = TestHelper::getData();
        TestHelper::setData($data + 1);
    }
}
